import requests
import os
from hello.pttbot.ck_test_server import run_chatbot
from django.shortcuts import render
from django.http import HttpResponse


# Create your views here.
def index(request):
    return render(request, 'index.html')

def query(request):
    sentence = request.GET.get('sentence', '')
    sentence = sentence.decode('utf-8').encode('utf-8')
    res = run_chatbot(sentence)     
    print('in query: ', sentence)
    print(res)      
    return HttpResponse(res)

