from django.conf.urls import include, url

from django.contrib import admin
admin.autodiscover()

import hello.views
import hello.pttbot.backend

# Examples:
# url(r'^$', 'gettingstarted.views.home', name='home'),
# url(r'^blog/', include('blog.urls')),

urlpatterns = [
    url(r'^$', hello.pttbot.backend.index, name='index'),
    url(r'^db', hello.views.db, name='db'),
    url(r'^admin/', include(admin.site.urls))
    #url(r'^query/$', hello.pttbot.backend.query, name='query'),
]
